/* ebc912399d92bbef1724fdd4c74a42a8a4535da2
 * This file is automatically generated by graphql-let. */

import * as Types from "graphql-let/__generated__/__types__";
import * as Apollo from '@apollo/client';
export declare type SettingsQueryVariables = Types.Exact<{
  [key: string]: never;
}>;
export declare type SettingsQuery = {
  __typename?: 'Query';
  settings: {
    __typename?: 'Setting';
    id: string;
    options: {
      __typename?: 'SettingsOptions';
      siteTitle: string;
      siteSubtitle: string;
      currency: string;
      minimumOrderAmount: number;
      currencyToWalletRatio?: number | null;
      signupPoints?: number | null;
      useOtp: boolean;
      taxClass: string;
      shippingClass: string;
      deliveryTime: Array<{
        __typename?: 'DeliveryTime';
        title: string;
        description: string;
      }>;
      logo: {
        __typename?: 'Attachment';
        id?: string | null;
        thumbnail?: string | null;
        original?: string | null;
      };
      seo: {
        __typename?: 'SeoSettings';
        metaTitle?: string | null;
        metaDescription?: string | null;
        ogTitle?: string | null;
        ogDescription?: string | null;
        twitterHandle?: string | null;
        twitterCardType?: string | null;
        metaTags?: string | null;
        canonicalUrl?: string | null;
        ogImage?: {
          __typename?: 'Attachment';
          id?: string | null;
          thumbnail?: string | null;
          original?: string | null;
        } | null;
      };
      google?: {
        __typename?: 'GoogleSettings';
        isEnable: boolean;
        tagManagerId: string;
      } | null;
      facebook?: {
        __typename?: 'FacebookSettings';
        isEnable: boolean;
        appId: string;
        pageId: string;
      } | null;
      contactDetails: {
        __typename?: 'ContactDetails';
        website: string;
        contact: string;
        socials: Array<{
          __typename?: 'ShopSocials';
          icon: string;
          url: string;
        }>;
        location: {
          __typename?: 'Location';
          lat?: number | null;
          lng?: number | null;
          formattedAddress?: string | null;
          city?: string | null;
          state?: string | null;
          country?: string | null;
          zip?: string | null;
        };
      };
    };
  };
  taxClasses: Array<{
    __typename?: 'Tax';
    id: string;
    name: string;
  }>;
  shippingClasses: Array<{
    __typename?: 'Shipping';
    id: string;
    name: string;
  }>;
};
export declare type UpdateSettingsMutationVariables = Types.Exact<{
  input: Types.SettingsInput;
}>;
export declare type UpdateSettingsMutation = {
  __typename?: 'Mutation';
  updateSettings: {
    __typename?: 'Setting';
    id: string;
    options: {
      __typename?: 'SettingsOptions';
      siteTitle: string;
      siteSubtitle: string;
      currency: string;
      minimumOrderAmount: number;
      taxClass: string;
      shippingClass: string;
      deliveryTime: Array<{
        __typename?: 'DeliveryTime';
        title: string;
        description: string;
      }>;
      logo: {
        __typename?: 'Attachment';
        id?: string | null;
        thumbnail?: string | null;
        original?: string | null;
      };
      seo: {
        __typename?: 'SeoSettings';
        metaTitle?: string | null;
        metaDescription?: string | null;
        ogTitle?: string | null;
        ogDescription?: string | null;
        twitterHandle?: string | null;
        twitterCardType?: string | null;
        metaTags?: string | null;
        canonicalUrl?: string | null;
        ogImage?: {
          __typename?: 'Attachment';
          id?: string | null;
          thumbnail?: string | null;
          original?: string | null;
        } | null;
      };
      google?: {
        __typename?: 'GoogleSettings';
        isEnable: boolean;
        tagManagerId: string;
      } | null;
      facebook?: {
        __typename?: 'FacebookSettings';
        isEnable: boolean;
        appId: string;
        pageId: string;
      } | null;
    };
  };
};
export declare const SettingsDocument: Apollo.DocumentNode;
/**
 * __useSettingsQuery__
 *
 * To run a query within a React component, call `useSettingsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSettingsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSettingsQuery({
 *   variables: {
 *   },
 * });
 */

export declare function useSettingsQuery(baseOptions?: Apollo.QueryHookOptions<SettingsQuery, SettingsQueryVariables>): Apollo.QueryResult<SettingsQuery, Types.Exact<{
  [key: string]: never;
}>>;
export declare function useSettingsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SettingsQuery, SettingsQueryVariables>): Apollo.LazyQueryResultTuple<SettingsQuery, Types.Exact<{
  [key: string]: never;
}>>;
export declare type SettingsQueryHookResult = ReturnType<typeof useSettingsQuery>;
export declare type SettingsLazyQueryHookResult = ReturnType<typeof useSettingsLazyQuery>;
export declare type SettingsQueryResult = Apollo.QueryResult<SettingsQuery, SettingsQueryVariables>;
export declare const UpdateSettingsDocument: Apollo.DocumentNode;
export declare type UpdateSettingsMutationFn = Apollo.MutationFunction<UpdateSettingsMutation, UpdateSettingsMutationVariables>;
/**
 * __useUpdateSettingsMutation__
 *
 * To run a mutation, you first call `useUpdateSettingsMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSettingsMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateSettingsMutation, { data, loading, error }] = useUpdateSettingsMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */

export declare function useUpdateSettingsMutation(baseOptions?: Apollo.MutationHookOptions<UpdateSettingsMutation, UpdateSettingsMutationVariables>): Apollo.MutationTuple<UpdateSettingsMutation, Types.Exact<{
  input: Types.SettingsInput;
}>, Apollo.DefaultContext, Apollo.ApolloCache<any>>;
export declare type UpdateSettingsMutationHookResult = ReturnType<typeof useUpdateSettingsMutation>;
export declare type UpdateSettingsMutationResult = Apollo.MutationResult<UpdateSettingsMutation>;
export declare type UpdateSettingsMutationOptions = Apollo.BaseMutationOptions<UpdateSettingsMutation, UpdateSettingsMutationVariables>;