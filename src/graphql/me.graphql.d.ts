/* 77b520ce7406390073e079824ea843129ef9dffe
 * This file is automatically generated by graphql-let. */

import * as Types from "graphql-let/__generated__/__types__";
import * as Apollo from '@apollo/client';
export declare type MeQueryVariables = Types.Exact<{
  [key: string]: never;
}>;
export declare type MeQuery = {
  __typename?: 'Query';
  me: {
    __typename?: 'User';
    id: string;
    name: string;
    email: string;
    is_active?: boolean | null;
    profile?: {
      __typename?: 'Profile';
      id: string;
      bio?: string | null;
      contact?: string | null;
      avatar?: {
        __typename?: 'Attachment';
        id?: string | null;
        thumbnail?: string | null;
        original?: string | null;
      } | null;
      socials?: Array<{
        __typename?: 'Social';
        type: string;
        link: string;
      }> | null;
    } | null;
    address?: Array<{
      __typename?: 'Address';
      id: string;
      title: string;
      default: boolean;
      type: Types.AddressType;
      address: {
        __typename?: 'UserAddress';
        country?: string | null;
        city?: string | null;
        state?: string | null;
        zip?: string | null;
        street_address?: string | null;
      };
    }> | null;
    wallet?: {
      __typename?: 'Wallet';
      id: string;
      total_points: number;
      points_used: number;
      available_points: number;
    } | null;
  };
};
export declare const MeDocument: Apollo.DocumentNode;
/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */

export declare function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>): Apollo.QueryResult<MeQuery, Types.Exact<{
  [key: string]: never;
}>>;
export declare function useMeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>): Apollo.LazyQueryResultTuple<MeQuery, Types.Exact<{
  [key: string]: never;
}>>;
export declare type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export declare type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export declare type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;